## Slide 1 - Alberto Fabbri

- Hi, my name is Alberto Fabbri
- I am a third year student of the Bachelor Programme in Software Development at Kristianstad University

## Slide 2 - Why software developer?

There are several motives why I want to be a software developer:
- I worked previously in the industrial automation field and I find out that I liked writing programs for PCs way more than doing the same for robots because PCs make it easier to experiment and rollback if things goes wrong.
- It is a big satisfaction to solve problems and to see things improve.
- It gives me the possibility to create new things such as websites and applications for smartphones.
- The Computer Science field is evolving at such a pace that there is always something new and exiting to try and learn.

## Slide 3 - What is most interesting to you?

- I love the concept behind the idea of automation, personally I feel like spending time on automating something is way wiser than spending the same time doing it manually. Obviously not everything can be automated but in computer science it is usually possible.
- Automation implies an augmented efficiency which turn out in better productivity with less effort.
- The constant improvements made possible by the fast changing IT landscape is the main reason why I like this field so much.
- For me it is really satisfactory to see something rise and get better every day.

## Slide 4 - How do you want to create impact as a software developer?

- I am a huge fan of open source tools because of the freedom they give people and the collaborative environment that surrounds every one of them. I think open source is the most impactful aspect of computer science because it allows people with new ideas to realize them re-using existing tools instead of having to do everything from scratch, which most of the time would be impossible.
- There are loads of such tools that have changed the world starting from the Linux kernel and Android to the apps developed by UNICEF and its partners to help the disadvantaged.

## Slide 5 - My thesis project plan

- One of my favorite sectors of computer science is DevOps. The main goal of it is to aid the other developers working in your team to develop new software more easily and to iterate faster. I think this is fascinating and when I heard about this new machine learning algorithm called "GitHub CoPilot" developed to bring code suggestions to the next level I was immediately curious.
- I think developers should focus on what really brings value and that if a new tool helps them in doing that they should adopt it. Therefore with my thesis project I decided to look into what GitHub CoPilot has to offer and how it changes the workflow of the developers adopting it.

## Conclusion

Thank you for listening to my presentation, have a nice day!